package LeetCode;
//網頁 : https://leetcode.com/problems/binary-tree-inorder-traversal/solution/
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Stack;

class Solution{
	public List<Integer> inorderTraversal(TreeNode root){
		List<Integer> res = new ArrayList<>();
		Stack<TreeNode> stack = new Stack<>();
		TreeNode curr = root;
		
		//為什麼要寫兩層?-----------------------------------------------------
		while(curr!=null || !stack.isEmpty()) {
			while(curr != null) {
				stack.push(curr);
				curr=curr.left;
			}
			
			curr = stack.pop();
			res.add(curr.val);
			curr = curr.right;
		}
		
		return res;
	}
}


public class Binary_tree_inorder_traversal_94 {

	public static void main(String[] args) throws IOException {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		String line;
		while ((line = in.readLine())!=null) {
			TreeNode root = stringToTreeNode(line);
			List<Integer> ret = new Solution().inorderTraversal(root);
			String out = integerArrayListToString(ret);
			System.out.println(out);
		}
	}

	
	public static TreeNode stringToTreeNode(String input) {
		input = input.trim();
		input = input.substring(1, input.length()-1);
		if(input.length()==0) {
			return null;
		}
		
		String[] parts = input.split(",");
		String item = parts[0];
		TreeNode root = new TreeNode(Integer.parseInt(item));
		Queue<TreeNode> nodeQueue = new LinkedList<>();
		nodeQueue.add(root);
		
		int index = 1;
		while(!nodeQueue.isEmpty()) {
			TreeNode node = nodeQueue.remove();
			
			if (index == parts.length) {
				break;
			}
			
			item = parts[index++];
			item = item.trim();
			if(!item.equals("null")) {
				int leftNumber =  Integer.parseInt(item);
				node.left = new TreeNode(leftNumber);
				nodeQueue.add(node.left);
			}
			
			if(index == parts.length) {
				break;
			}
			
			item = parts[index++];
			item = item.trim();
			if(!item.equals("null")) {
				int rightNumber = Integer.parseInt(item);
				node.right = new TreeNode(rightNumber);
				nodeQueue.add(node.right);
			}
			
		}
		
		return root;
	}
	
	public static String integerArrayListToString(List<Integer> nums, int length) {
		if(length == 0) {
			return "[]";
		}
		
		String result = "";
		for(int index=0; index<length; index++) {
			Integer number = nums.get(index);
			result += Integer.toString(number)+", ";
		}
		
		System.out.println("result.substring(0, result.length() - 2)="+result.substring(0, result.length() - 2));
		//輸出 : result.substring(0, result.length() - 2)=1, 3, 2
		//為什麼要 -2?
		
		return "["+result.substring(0, result.length()-2)+"]";
	}
	
	public static String integerArrayListToString(List<Integer> nums) {
		return integerArrayListToString(nums, nums.size());
	}
	
}













