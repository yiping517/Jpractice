package recursive;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class recursive_Hanoi {

	public static void main(String[] args) throws Exception{

		int n;
		BufferedReader buf = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.print("請輸入盤數");
		n = Integer.parseInt(buf.readLine());
		
		recursive_Hanoi hanoi = new recursive_Hanoi();
		hanoi.move(n, 'A', 'B', 'C');
	}

	public void move(int n, char a, char b, char c) { 
		if(n==1) {
			System.out.println("盤"+n+"由"+a+"移至"+c);
		} else {
			move(n-1, a, c, b);
			System.out.println("盤"+n+"由"+a+"移至"+c);
			move(n-1, b, a, c);
		}
		
	}
	//當n=2:(2,'A','B','C')
	//跑else->move(1,'A','C','B')->跑if->印出:(盤1由'A'移至'B')->印出:(盤2由'A'移至'C')->move(1,'A','B','C')->印出:(盤1由B移至C)
																					// b = 'A'
																					// a = 'B'
																					// c = 'C'
}
















