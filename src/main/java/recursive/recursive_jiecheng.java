package recursive;

public class recursive_jiecheng {

	public static void main(String[] args) {
		int n = 5;
		System.out.println(jiecheng(n));
	}
	
	public static int jiecheng(int n) {
		if(n!=0) {
			return n*jiecheng(n-1);
		}else {
			return 1;
		}
	}

	// 5*jiecheng(4)
		// 4*jiecheng(3)
				//3*jiecheng(2)
						//2*jiecheng(1)
							// 1*jiechen(0)
									//1
}
