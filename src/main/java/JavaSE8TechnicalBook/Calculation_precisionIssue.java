package JavaSE8TechnicalBook;

import java.math.BigDecimal;

public class Calculation_precisionIssue {

	public static void main(String[] args) {

		double result = new Calculation_precisionIssue().sumup(0.1, 0.1, 0.1);
		if(result==0.3) {
			System.out.println("使用double型態時,==0.3");
		}else {
			System.out.println("使用double型態時,不等於0.3");
		}
		
		System.out.println("------------------------------------------");
		
		BigDecimal n1 = new BigDecimal(0.1);
		BigDecimal n2 = new BigDecimal(0.1);
		BigDecimal result2 = new BigDecimal(0.2);//=>System.out.println("使用BigDecimal型態時,不等於0.2");
//		BigDecimal n1 = new BigDecimal("0.1");
//		BigDecimal n2 = new BigDecimal("0.1");
//		BigDecimal result2 = new BigDecimal("0.2"); //=>"使用BigDecimal型態時,==0.2"
		if((n1.add(n2)).equals(result2)) {
			System.out.println("使用BigDecimal型態時,==0.2");
		}else {
			System.out.println("使用BigDecimal型態時,不等於0.2");
		}

	}
	
	public double sumup(double a, double b, double c) {
		return a+b+c;
	}
}
