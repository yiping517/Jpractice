package JavaSE8TechnicalBook.tryCatch;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Average3_while {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		double sum = 0;
		int count = 0;
		while(true) {
			try {
				int number = scan.nextInt();
				if(number == 0) {
					break;
				}
				System.out.print("number="+number+",");
				sum+=number;
				count++;
			}catch(InputMismatchException ex) {
				System.out.printf("略過非整數輸入:%s%n",scan.next());
			}
		}
		System.out.println("count="+count);
		System.out.printf("平均 %.2f%n",sum/count);
	}

}
