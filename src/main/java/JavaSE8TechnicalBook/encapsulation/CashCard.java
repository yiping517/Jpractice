package JavaSE8TechnicalBook.encapsulation;

public class CashCard {

	//卡號
	private String cardId;
	//餘額
	private int balance;
	//點數
	private int bonus;
	//-----------------------------------------------------------------------------------
	
	//存錢
	public void save(int amount) {
		if(amount>0) {
			balance += amount;
			
			//若存超過16,000   就予點數 16點
			if(amount >= 16000) {
				bonus += 16;
			}
			
		}else {
			System.out.println("輸入錯誤");
		}
		System.out.println("儲存作業結束");
	}
	
	
	//--------------------------------------------------------------------------------------
	public CashCard() {
		System.out.println("CashCard()");
	}
	
	public CashCard(String cardId, int balance, int bonus) {
		this.cardId = cardId;
		this.balance = balance;
		this.bonus = bonus;
	}
	//---------------------------------------------------------------------------------------
//	public void setCardId(String cardId) {
//		this.cardId = cardId;
//	}
	
	public String getCardId() {
		return cardId;
	}
	
//	public void setBalance(int balance) { //可是可以設定餘額好像有點怪????????????????????????????????
//		this.balance = balance;
//	}
	
	public int getBalance() {
		return balance;
	}
	
//	public void setBonus(int bonus) {
//		this.bonus = bonus;
//	}
	public int getBonus() {
		return bonus;
	}
	
	public String toString() { //這個方法需不需要參數?????????????????????????????????
		return "[卡號是 : "+getCardId()+"; 餘額是 : "+getBalance()+" ; 紅利是 : "+getBonus()+"]";
	}
}
