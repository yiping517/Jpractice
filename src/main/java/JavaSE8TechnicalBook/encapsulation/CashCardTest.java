package JavaSE8TechnicalBook.encapsulation;

import java.util.Scanner;

public class CashCardTest {

	public static void main(String[] args) {

		CashCard cc1 = new CashCard("1",645678,6789);
		CashCard cc2 = new CashCard("2",543236,5678);
		CashCard cc3 = new CashCard("3",262389,2345);
		
		Scanner scan = new Scanner(System.in);
		System.out.println("請輸入您的卡號:");
		String cardId = scan.next();
		System.out.println("您所輸入的卡號是 : "+cardId);
		switch(cardId) {
		case "1":
			
			System.out.println("cc1:"+cc1);
			break;
		case "2":
			
			System.out.println("cc2:"+cc2);
			break;
		case "3":
			
			System.out.println("cc3:"+cc3);
			break;
		case "0":break;
		default :	
			System.out.println("所輸入的卡號不存在");
			System.out.println("請再次輸入卡號,或輸入0離開");
			cardId = scan.next();
		}
		
		
		System.out.println("請選擇所執行的作業代號: 1:存款 ; 2.查詢餘額"); 
		int code = scan.nextInt();
		
		switch(code) {
		case 1:
			System.out.println("存款作業進行中----");
			System.out.println("請輸入所欲存款的金額:");
			System.out.println(cc1.toString());
			int amount = scan.nextInt();
			cc1.save(amount);
			System.out.println(cc1.toString());
		}
	}

}
