package JavaSE8TechnicalBook;

public class RandomStop2_while {

	public static void main(String[] args) {
		
		int number = (int)(Math.random()*10);
		while(number !=5 ) {
			number = (int)(Math.random()*10);
			System.out.println(number);
		}
	}

}
