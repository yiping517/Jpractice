package JavaSE8TechnicalBook;


public class Range_Expression {

	public static void main(String[] args) {

		System.out.println("Byte:");
		System.out.println(Byte.MIN_VALUE+"~"+Byte.MAX_VALUE);//-128~127
		System.out.printf("%d ~ %d%n",Byte.MIN_VALUE,Byte.MAX_VALUE);//-128 ~ 127
		
		System.out.println("--------------------------------------------");
		
		System.out.println("Short:");
		System.out.println(Short.MIN_VALUE+"~"+Short.MAX_VALUE);//-32768~32767
		System.out.printf("%d ~ %d%n",Short.MIN_VALUE,Short.MAX_VALUE);//-32768 ~ 32767
		
		System.out.println("--------------------------------------------");
		
		System.out.println("Integer:");
		System.out.println(Integer.MIN_VALUE+"~"+Integer.MAX_VALUE);//-2147483648~2147483647
		System.out.printf("%d ~ %d%n",Integer.MIN_VALUE,Integer.MAX_VALUE);//-2147483648 ~ 2147483647
		
		System.out.println("--------------------------------------------");
		
		System.out.println("Long:");
		System.out.println(Long.MIN_VALUE+"~"+Long.MAX_VALUE);//-9223372036854775808~9223372036854775807
		System.out.printf("%d ~ %d%n",Long.MIN_VALUE,Long.MAX_VALUE);//-9223372036854775808 ~ 9223372036854775807
		
		System.out.println("-------float, double精度範圍-------------------------------------");
		
		System.out.println("Float:");
		System.out.println(Float.MIN_EXPONENT+","+Float.MAX_EXPONENT);//-126,127
		System.out.printf("%d ~ %d%n", Float.MIN_EXPONENT,Float.MAX_EXPONENT);//-126 ~ 127
//		System.out.printf("%d ~ %d%n", Float.MIN_VALUE,Float.MAX_VALUE); //java.util.IllegalFormatConversionException: d != java.lang.Float
		System.out.printf("%f ~ %f%n", Float.MIN_VALUE,Float.MAX_VALUE); //0.000000 ~ 340282346638528860000000000000000000000.000000
		
		
		System.out.println("--------------------------------------------");
		System.out.println("char可表示的Unicode範圍");
		System.out.printf("%h ~ %h%n", Character.MIN_VALUE,Character.MAX_VALUE);//0 ~ ffff
		
		
	}

}
