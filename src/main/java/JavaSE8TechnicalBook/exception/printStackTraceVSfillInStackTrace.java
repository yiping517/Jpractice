package JavaSE8TechnicalBook.exception;

public class printStackTraceVSfillInStackTrace {

	public static void main(String[] args) {
		try {
			c();
		}catch(NullPointerException ex) {
			ex.printStackTrace();
		}
	}
	static void c() {
		try {
			b();
		}catch(NullPointerException ex) {
//			ex.printStackTrace();
//			throw ex;
//			��X:
//			java.lang.NullPointerException
//			at JavaSE8TechnicalBook.exception.PrintStackTrace.a(PrintStackTrace.java:27)
//			at JavaSE8TechnicalBook.exception.PrintStackTrace.b(PrintStackTrace.java:22)
//			at JavaSE8TechnicalBook.exception.PrintStackTrace.c(PrintStackTrace.java:14)
//			at JavaSE8TechnicalBook.exception.PrintStackTrace.main(PrintStackTrace.java:7)
			
			ex.printStackTrace();
			Throwable t = ex.fillInStackTrace();
			throw (NullPointerException)t;
//			��X:
//			java.lang.NullPointerException
//			at JavaSE8TechnicalBook.exception.printStackTraceVSfillInStackTrace.c(printStackTraceVSfillInStackTrace.java:26)
//			at JavaSE8TechnicalBook.exception.printStackTraceVSfillInStackTrace.main(printStackTraceVSfillInStackTrace.java:7)
		}
	}
	
	static void b() {
		a();
	}
	
	static String a() {
		String text = null;
		return text.toUpperCase();
	}
}
