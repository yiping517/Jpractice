package JavaSE8TechnicalBook;

import java.util.Random;

public class RandomStop2_if {

	public static void main(String[] args) {
//		int i=0;
//		System.out.println(i);
//		int number = (int)(Math.random()*10);
//		System.out.println(number);
//		if(number != 5) {
//			i++;
//			number = (int)(Math.random()*10);
//		}
//		上面這段代碼不會循環. 只會跑一次而已.
		
		while(true) {
//			int number = (int)(Math.random()*10);
//			Random r = new Random(10);
			Random r = new Random();
			int number = r.nextInt(10);
			System.out.println(number);
			if(number == 5) {
				break;
			}
		}
	}

}
