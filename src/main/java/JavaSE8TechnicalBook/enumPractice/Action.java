package JavaSE8TechnicalBook.enumPractice;

public enum Action {
	STOP,
	RIGHT,
	LEFT,
	UP,
	DOWN
}
