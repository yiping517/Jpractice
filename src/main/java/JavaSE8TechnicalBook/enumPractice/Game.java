package JavaSE8TechnicalBook.enumPractice;
import static java.lang.System.out;

public class Game {

	public static void main(String[] args) {
		play(Action.RIGHT);
		play(Action.UP);
	}
	
	public static void play(Action action) {//宣告為Action型態
		
		switch(action) {
		//列舉Action實例
		case STOP://也就是Action.STOP
			out.println("播放停止動畫");
			break;
		case RIGHT:
			out.println("播放向右動畫");
			break;
		case LEFT:
			out.println("播放向左動畫");
			break;
		case UP:
			out.println("播放向上動畫");
			break;
		case DOWN:
			out.println("播放向下動畫");
			break;
		}
	}
}
