package JavaSE8TechnicalBook.interfacePolymorphism;

public class Client {

	public final String ip;
	public final String name;
	
//	public Client() {
//		System.out.println("Client()");
//	}
//	上面會出現以下錯誤 : 
//	Multiple markers at this line
//	- The blank final field name may not have been initialized
//	- The blank final field ip may not have been initialized
	
	public Client(String ip, String name) {
		this.ip = ip;
		this.name = name;
	}
}
