package JavaSE8TechnicalBook.interfacePolymorphism;

public class MultiChat {
	public static void main(String[] args) {
		Client c1 = new Client("127.0.0.1","CC");
		Client c2 = new Client("192.168.0.0","SS");
		
		ClientQueue queue = new ClientQueue();
		//-------------------------------------想!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		queue.addClientListener(new ClientListener() {
			@Override
			public void clientAdded(ClientEvent event) {
				System.out.printf("%s 從  %s 連線%n",event.getName(),event.getIp());
			}
			@Override
			public void clientRemove(ClientEvent event) {
				System.out.printf("%s 從 %s 離線%n", event.getName(),event.getIp());
			}
		});
		
		queue.add(c1);
		queue.add(c2);
		queue.remove(c2);
	}
}
