package JavaSE8TechnicalBook.interfacePolymorphism;

public interface ClientListener {

	void clientAdded(ClientEvent event);//新增Client會呼叫這個方法
	void clientRemove(ClientEvent event);//移除Client會呼叫這個方法
}
