package JavaSE8TechnicalBook.interfacePolymorphism;

import java.util.ArrayList;

public class ClientQueue {

	//收集連線的Client
	private ArrayList clients = new ArrayList();
	
	//收集對ClientQueue有興趣的ClientListener
	private ArrayList listeners = new ArrayList();
	
	//註冊ClientListener
	public void addClientListener(ClientListener listener) {
		listeners.add(listener);
	}
	
	public void add(Client client) {
		//新增Client
		clients.add(client);
		
		//通知資訊包裝為ClientEvent
		ClientEvent event = new ClientEvent(client);
		
		//逐一取出ClientListener通知
		for(int i=0; i<listeners.size();i++) {
			ClientListener listener = (ClientListener)listeners.get(i);
			listener.clientAdded(event);
		}
	}
	
	public void remove(Client client) {
		clients.remove(client);
		ClientEvent event = new ClientEvent(client);
		for(int i=0;i<listeners.size();i++) {
			ClientListener listener =(ClientListener)listeners.get(i);
			listener.clientRemove(event);
		}
	}
	
}
