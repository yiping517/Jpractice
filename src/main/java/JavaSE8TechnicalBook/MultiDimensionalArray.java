package JavaSE8TechnicalBook;

public class MultiDimensionalArray {

	public static void main(String[] args) {

		int[][] mda = {
				{6,8,3,4},
				{9,0,1,2},
				{2,1,7,2}
		};
		
		for(int[] md:mda) {
			System.out.println("----int[]------------");
			System.out.println(md[md.length-1]);
			for(int m : md) {
				System.out.println("-----int-----");
				System.out.println(m);
			}
		}
//		----int[]------------
//		4
//		-----int-----
//		6
//		-----int-----
//		8
//		-----int-----
//		3
//		-----int-----
//		4
//		----int[]------------
//		2
//		-----int-----
//		9
//		-----int-----
//		0
//		-----int-----
//		1
//		-----int-----
//		2
//		----int[]------------
//		2
//		-----int-----
//		2
//		-----int-----
//		1
//		-----int-----
//		7
//		-----int-----
//		2
		
		
	}

}
