package JavaSE8TechnicalBook;

public class Wrapper {

	public static void main(String[] args) {

		int n1 = 10;
		int n2 = 20;
		
		Integer i1 = new Integer(n1);
		Integer i2 = new Integer(n2);
		
		System.out.println(n1/3);//3
		System.out.println(i1.intValue()/3);//3
		System.out.println(i2.compareTo(i1));//1
	}

}
